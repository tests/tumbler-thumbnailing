#!/bin/sh
# vim: set sts=4 sw=4 et tw=0 :

set -e

TESTDIR=$(cd $(dirname $0); pwd; cd - >/dev/null 2>&1)
. "${TESTDIR}/config.sh"

. "${TESTDIR}/common/update-test-path"

MEDIA_RESOURCE_DIR="$(cd "${TESTDIR}/resources/media" && pwd)"

#########
# Setup #
#########
trap "setup_failure" EXIT

set -x
# Need a DBus session for this test
ensure_dbus_session
set +x

setup_success

###########
# Execute #
###########
_launch_monitor() {
    # Launch tumblerd manually to allow LD_PRELOAD to work for 
    # chaiwala-apparmor-tumbler-tests
    pkill tumblerd || true
    /usr/lib/*-linux-gnu*/tumbler-1/tumblerd &
    TUMBLERD_PID=$!
    sleep 5
    # Launch and monitor the thumbnailer
    ${GDBUS} monitor --session --dest "${addr}" --object-path "${obj_path}" >"${logfile}" &
    GDBUS_MONITOR_PID=$!
}

_kill_monitor_return() {
    kill -s TERM $GDBUS_MONITOR_PID || true
    kill -s TERM $TUMBLERD_PID || true
    return $1
}

stringlist_to_gvariant_as() {
    local i converted="['$1'"
    shift
    for i in $@; do
        converted="$converted, '$i'"
    done
    converted="$converted]"
    echo "${converted}"
}

_check_uris_have_thumbnail() {
    local size=$1
    shift
    for i in $@; do
        local thumb="${HOME}/.cache/thumbnails/${size}/$(echo -n "${i}" | md5sum | cut -f1 -d\ ).png"
        say $thumb
        if [ ! -f "${thumb}" ]; then
            whine "Couldn't find thumbnail $thumb, file $i didn't get thumbnailed!?"
            ret=1
        fi
    done
}

_generate_thumbnails() {
    set -x
    local i ret files logfile addr obj_path method uris filetypes
    local copy
    ret=0
    local size="$1"
    local special_dir="$2"
    shift 2
    files=$@
    logfile="${WORKDIR}/monitor-tumblerd.log"
    addr="org.freedesktop.thumbnails.Thumbnailer1"
    obj_path="/org/freedesktop/thumbnails/Thumbnailer1"
    method="org.freedesktop.thumbnails.Thumbnailer1.Queue"
    uris=""
    filetypes=""

    # FIXME: This doesn't do whitespace/special character escaping, etc
    for i in ${files}; do
        # Tumbler's AppArmor profile doesn't necessarily let it read the
        # apertis-tests directory if we're running uninstalled. Copy the
        # file to a more realistic location, which exercises the AppArmor
        # profile better anyway.
        _rand=$(od -A n -N 2 -t u2 /dev/urandom)
        if [ $(( $_rand % 2 )) = 0 ] && [ -e "/home/shared/$special_dir" ]; then
            copy="/home/shared/$special_dir/apertis-tests__$(basename "$i")"
        else
            copy="$HOME/$special_dir/apertis-tests__$(basename "$i")"
        fi

        cp -v "$i" "$copy"

        # Need path relative to /
        uris="${uris} file://$copy"
        filetypes="${filetypes} $(file --mime-type "$copy" | cut -d ":" -f 2 | tr -d ' ')"
    done

    # Clear out old thumbnails.
    rm -rf ${HOME}/.cache/thumbnails/

    _launch_monitor

    # Files to thumbnail
    ${GDBUS} call --session --dest "${addr}" --object-path "${obj_path}" \
        --method "${method}" \
        "$(stringlist_to_gvariant_as ${uris})" \
        "$(stringlist_to_gvariant_as ${filetypes})" \
        "${size}" foreground 0

    # Wait for thumbnailing to finish
    # FIXME: Race condition! Fix me to loop on the monitor log file.
    _sleep 2
    _check_uris_have_thumbnail $size $uris || ret=1

    rm -f "/home/shared/$special_dir"/apertis-tests__*
    rm -f "${HOME}/$special_dir"/apertis-tests__*

    _kill_monitor_return $ret
    set +x
}

test_image_normal_thumbnail_generation() {
    _generate_thumbnails normal Pictures "${MEDIA_RESOURCE_DIR}/images"/*
}

test_image_large_thumbnail_generation() {
    _generate_thumbnails large Pictures "${MEDIA_RESOURCE_DIR}/images"/*
}

test_video_normal_thumbnail_generation() {
    _generate_thumbnails normal Videos "${MEDIA_RESOURCE_DIR}/videos"/*
}

test_video_large_thumbnail_generation() {
    _generate_thumbnails large Videos "${MEDIA_RESOURCE_DIR}/videos"/*
}

test_document_normal_thumbnail_generation() {
    _generate_thumbnails normal Documents "${MEDIA_RESOURCE_DIR}/documents"/*.{pdf,odt}
}

test_document_large_thumbnail_generation() {
    _generate_thumbnails large Documents "${MEDIA_RESOURCE_DIR}/documents"/*.{pdf,odt}
}

trap "test_failure" EXIT

# NOTE: Tumbler cannot thumbnail audio files
src_test_pass <<-EOF
test_image_normal_thumbnail_generation
test_image_large_thumbnail_generation
test_document_normal_thumbnail_generation
test_document_large_thumbnail_generation
test_video_normal_thumbnail_generation
test_video_large_thumbnail_generation
EOF

test_success
